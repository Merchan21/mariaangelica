package facci.examen.com;

public class Modelo {

    String cedula, fecha, hora, minuto, segundo;

    public Modelo(String cedula, String fecha, String hora, String minuto, String segundo) {
        this.cedula = cedula;
        this.fecha = fecha;
        this.hora = hora;
        this.minuto = minuto;
        this.segundo = segundo;
    }

    public String getCedula() {
        return cedula;
    }

    public String getFecha() {
        return fecha;
    }

    public String getHora() {
        return hora;
    }

    public String getMinuto() {
        return minuto;
    }

    public String getSegundo() {
        return segundo;
    }
}
