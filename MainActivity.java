package facci.examen.com;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import facci.examen.com.Adappter.MarketAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    TextView cedula, fecha, hora, minuto, segundo;
    Button enviar, general, individual;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cedula = findViewById(R.id.cedula);
        fecha = findViewById(R.id.fecha);
        hora = findViewById(R.id.hora);
        minuto = findViewById(R.id.minuto);
        segundo = findViewById(R.id.segundo);
        enviar = findViewById(R.id.enviar);
        general = findViewById(R.id.consultageneral);
        individual = findViewById(R.id.consultaindividual);



        enviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                subir();


            }
        });
        general.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, Main2Activity.class));


            }
        });
        individual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



            }
        });




    }

    private void subir() {

        MarketAdapter adapter = new MarketAdapter();
        Call<Modelo> call = adapter.InsertPost(new Modelo(cedula.getText().toString(), fecha.getText().toString(),hora.getText().toString(), minuto.getText().toString(),segundo.getText().toString()));
        call.enqueue(new Callback<Modelo>() {
            @Override
            public void onResponse(Call<Modelo> call, Response<Modelo> response) {
                Log.e("Hola", response.body().toString());
            }

            @Override
            public void onFailure(Call<Modelo> call, Throwable t) {

            }
        });


    }
}
