package facci.examen.com;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class AdaptadorConsulta extends RecyclerView.Adapter<AdaptadorConsulta.ViewHolderImagenes> {
    ArrayList<Modelo> listaImagenes;

    public AdaptadorConsulta(ArrayList<Modelo> listaImagenes) {
        this.listaImagenes = listaImagenes;
    }

    @NonNull
    @Override
    public AdaptadorConsulta.ViewHolderImagenes onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item, null, false);
        return new ViewHolderImagenes(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdaptadorConsulta.ViewHolderImagenes holder, int position) {

        holder.cedula.setText(listaImagenes.get(position).getCedula());
        holder.fecha.setText(listaImagenes.get(position).getFecha());
        holder.hora.setText(listaImagenes.get(position).getHora());
        holder.minuto.setText(listaImagenes.get(position).getMinuto());
        holder.segundo.setText(listaImagenes.get(position).getSegundo());





    }

    @Override
    public int getItemCount() {
        return listaImagenes.size();
    }

    public class ViewHolderImagenes extends RecyclerView.ViewHolder {

        TextView cedula, fecha, hora, minuto, segundo;

        public ViewHolderImagenes(View itemView) {
            super(itemView);

            cedula = (TextView) itemView.findViewById(R.id.cedula);
            fecha = (TextView) itemView.findViewById(R.id.fecha);
            hora = (TextView) itemView.findViewById(R.id.hora);
            minuto = (TextView) itemView.findViewById(R.id.minuto);
            segundo = (TextView) itemView.findViewById(R.id.segundo);

        }
    }
}



