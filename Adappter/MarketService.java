package facci.examen.com.Adappter;

import java.util.List;

import facci.examen.com.ApiConstants;
import facci.examen.com.Modelo;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface MarketService {
    @POST(ApiConstants.MARKET_POST_ENDPOINT)
    Call<Modelo> InsertPost(@Body Modelo post);

    @GET(ApiConstants.MARKET_POST_ENDPOINT)
    Call<List<Modelo>> getPost();
}
