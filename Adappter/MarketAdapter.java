package facci.examen.com.Adappter;


import java.util.List;

import facci.examen.com.ApiConstants;
import facci.examen.com.Modelo;
import retrofit2.Call;

public class MarketAdapter extends BaseAdapter implements MarketService{
    private MarketService marketService;

    public MarketAdapter(){
        super(ApiConstants.BASE_POST_URL);
        marketService=creativeService(MarketService.class);
    }
    @Override
    public Call<Modelo> InsertPost(Modelo post) {
        return marketService.InsertPost(post);
    }
    @Override
    public Call<List<Modelo>> getPost() {
        return marketService.getPost();
    }
}



