package facci.examen.com;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import facci.examen.com.Adappter.MarketAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Main2Activity extends AppCompatActivity {

    ArrayList<Modelo> listaImagenes;

    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        listaImagenes = new ArrayList<>();
        recyclerView = (RecyclerView)findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        mostrarImagenes();

    }

    private void mostrarImagenes() {
        MarketAdapter adapter = new MarketAdapter();
        Call<List<Modelo>> call = adapter.getPost();
        call.enqueue(new Callback<List<Modelo>>() {
            @Override
            public void onResponse(Call<List<Modelo>> call, Response<List<Modelo>> response) {
                List<Modelo> lista = response.body();
                for (Modelo post: lista
                ) {
                    listaImagenes.add(post);
                }
                AdaptadorConsulta adaptador_imagenes = new AdaptadorConsulta(listaImagenes);
                recyclerView.setAdapter(adaptador_imagenes);
            }

            @Override
            public void onFailure(Call<List<Modelo>> call, Throwable t) {

            }
        });
    }
}
